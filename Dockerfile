FROM image-registry.openshift-image-registry.svc:5000/elyusron-dev/ubi8-php-74

COPY index.html /var/www/html




#USER 0:0
COPY --chown=apache:apache . /var/www/html
COPY --chown=apache:apache httpd.conf /etc/httpd/conf/
#WORKDIR /var/www/html
#EXPOSE 8080
#EXPOSE 8443
ENTRYPOINT ["/usr/sbin/httpd", "-D", "FOREGROUND"]
